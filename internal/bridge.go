package internal

import (
	"os"

	"github.com/brutella/hc"
	"github.com/brutella/hc/accessory"
	"github.com/brutella/hc/log"
	"github.com/mdp/qrterminal/v3"

	ipx800v4 "gitlab.com/tfournier/ipx800v4-homekit-bridge/pkg/gce/ipx800/v4"
)

type Config struct {
	Port        string              `json:"port" yaml:"port" default:"9898"`
	Pin         string              `json:"pin" yaml:"pin" default:"00102003"`
	SetupID     string              `json:"setup_id" yaml:"setup_id" default:"HOME"`
	StoragePath string              `json:"storage_path" yaml:"storage_path" default:"./cache"`
	IPX         ipx800v4.Controller `json:"ipx" yaml:"ipx"`
	Accessories Accessories         `json:"accessories" yaml:"accessories"`
}

type Bridge interface {
	Start()
}

func NewBridge(cfg *Config) error {
	config := hc.Config{
		Port:        cfg.Port,
		Pin:         cfg.Pin,
		SetupId:     cfg.SetupID,
		StoragePath: cfg.StoragePath,
	}

	bridge := accessory.NewBridge(accessory.Info{
		Manufacturer: "GCE ELECTRONICS",
		Model:        "IPX-800 - V4",
		Name:         "Controller",
	})

	t, err := hc.NewIPTransport(config, bridge.Accessory, cfg.Accessories.List(cfg.IPX)...)
	if err != nil {
		panic(err)
	}

	hc.OnTermination(func() {
		<-t.Stop()
	})

	uri, err := t.XHMURI()
	if err != nil {
		return err
	}

	log.Info.Printf("HomeKit Pin: %s", config.Pin)
	log.Info.Printf("Setup Payload: %s", uri)

	qrterminal.GenerateWithConfig(uri, qrterminal.Config{
		Level:          qrterminal.M,
		Writer:         os.Stdout,
		BlackChar:      qrterminal.WHITE,
		BlackWhiteChar: qrterminal.BLACK_BLACK,
		WhiteChar:      qrterminal.BLACK,
		WhiteBlackChar: qrterminal.WHITE_BLACK,
		QuietZone:      0,
	})

	t.Start()

	return nil
}
