package internal

import (
	"fmt"

	"github.com/brutella/hc/accessory"
	"github.com/brutella/hc/log"

	ipx800v4 "gitlab.com/tfournier/ipx800v4-homekit-bridge/pkg/gce/ipx800/v4"
)

const (
	relaysLengthPerExtension = 8
)

type Relay struct {
	ID      int    `json:"id" yaml:"id"`
	Name    string `json:"name" yaml:"name"`
	Inverse bool   `json:"inverse" yaml:"inverse"`
}

func (r Relay) IsValid() bool {
	return 1 <= r.ID && r.ID <= 56
}

func (r Relay) Accessory(ctr ipx800v4.Controller) *accessory.Accessory {
	if r.IsValid() {
		info := accessory.Info{
			Name:         r.Name,
			SerialNumber: fmt.Sprintf("R%0.2d", r.ID),
			Manufacturer: "GCE ELECTRONICS",
			Model:        "IPX-800 - V4",
		}

		if i := (r.ID - 1) / relaysLengthPerExtension; i > 0 {
			info.Model = fmt.Sprintf("X-8R - %d", i)
		}

		acc := accessory.NewOutlet(info)

		acc.Outlet.On.OnValueRemoteGet(func() bool {
			if res, err := ctr.GetRelayState(r.ID); err == nil {
				return res
			}

			return false
		})

		acc.Outlet.On.OnValueRemoteUpdate(func(b bool) {
			if b {
				_ = ctr.SetRelayOn(r.ID)
			} else {
				_ = ctr.SetRelayOff(r.ID)
			}
		})

		log.Info.Printf("Load %#v", info)

		return acc.Accessory
	}

	return nil
}
