package internal

import (
	"github.com/brutella/hc/accessory"

	ipx800v4 "gitlab.com/tfournier/ipx800v4-homekit-bridge/pkg/gce/ipx800/v4"
)

type Accessories struct {
	Relays []Relay `json:"relays" yaml:"relays"`
}

func (a Accessories) List(ctr ipx800v4.Controller) []*accessory.Accessory {
	var as []*accessory.Accessory

	for _, r := range a.Relays {
		if acc := r.Accessory(ctr); acc != nil {
			as = append(as, acc)
		}
	}

	return as
}
