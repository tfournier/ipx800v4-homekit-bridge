module gitlab.com/tfournier/ipx800v4-homekit-bridge

go 1.15

require (
	github.com/brutella/hc v1.2.3
	github.com/jinzhu/configor v1.2.0
	github.com/mdp/qrterminal/v3 v3.0.0
)
