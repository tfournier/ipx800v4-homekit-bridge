package main

import (
	"github.com/brutella/hc/log"
	"github.com/jinzhu/configor"
	"gitlab.com/tfournier/ipx800v4-homekit-bridge/internal"
)

func main() {
	log.Info.Printf("Load config file %s", "./config.yaml")

	cfg := &internal.Config{}
	if err := configor.New(&configor.Config{Silent: true}).Load(cfg, "config.yaml"); err != nil {
		log.Info.Fatal(err)
	}

	if err := internal.NewBridge(cfg); err != nil {
		log.Info.Fatal(err)
	}
}
