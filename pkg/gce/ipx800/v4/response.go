package v4

type Response struct {
	Product           string  `json:"product"`
	Status            string  `json:"status"`
	R1                int     `json:"R1,omitempty"`
	R2                int     `json:"R2,omitempty"`
	R3                int     `json:"R3,omitempty"`
	R4                int     `json:"R4,omitempty"`
	R5                int     `json:"R5,omitempty"`
	R6                int     `json:"R6,omitempty"`
	R7                int     `json:"R7,omitempty"`
	R8                int     `json:"R8,omitempty"`
	R9                int     `json:"R9,omitempty"`
	R10               int     `json:"R10,omitempty"`
	R11               int     `json:"R11,omitempty"`
	R12               int     `json:"R12,omitempty"`
	R13               int     `json:"R13,omitempty"`
	R14               int     `json:"R14,omitempty"`
	R15               int     `json:"R15,omitempty"`
	R16               int     `json:"R16,omitempty"`
	R17               int     `json:"R17,omitempty"`
	R18               int     `json:"R18,omitempty"`
	R19               int     `json:"R19,omitempty"`
	R20               int     `json:"R20,omitempty"`
	R21               int     `json:"R21,omitempty"`
	R22               int     `json:"R22,omitempty"`
	R23               int     `json:"R23,omitempty"`
	R24               int     `json:"R24,omitempty"`
	R25               int     `json:"R25,omitempty"`
	R26               int     `json:"R26,omitempty"`
	R27               int     `json:"R27,omitempty"`
	R28               int     `json:"R28,omitempty"`
	R29               int     `json:"R29,omitempty"`
	R30               int     `json:"R30,omitempty"`
	R31               int     `json:"R31,omitempty"`
	R32               int     `json:"R32,omitempty"`
	R33               int     `json:"R33,omitempty"`
	R34               int     `json:"R34,omitempty"`
	R35               int     `json:"R35,omitempty"`
	R36               int     `json:"R36,omitempty"`
	R37               int     `json:"R37,omitempty"`
	R38               int     `json:"R38,omitempty"`
	R39               int     `json:"R39,omitempty"`
	R40               int     `json:"R40,omitempty"`
	R41               int     `json:"R41,omitempty"`
	R42               int     `json:"R42,omitempty"`
	R43               int     `json:"R43,omitempty"`
	R44               int     `json:"R44,omitempty"`
	R45               int     `json:"R45,omitempty"`
	R46               int     `json:"R46,omitempty"`
	R47               int     `json:"R47,omitempty"`
	R48               int     `json:"R48,omitempty"`
	R49               int     `json:"R49,omitempty"`
	R50               int     `json:"R50,omitempty"`
	R51               int     `json:"R51,omitempty"`
	R52               int     `json:"R52,omitempty"`
	R53               int     `json:"R53,omitempty"`
	R54               int     `json:"R54,omitempty"`
	R55               int     `json:"R55,omitempty"`
	R56               int     `json:"R56,omitempty"`
	D1                int     `json:"D1,omitempty"`
	D2                int     `json:"D2,omitempty"`
	D3                int     `json:"D3,omitempty"`
	D4                int     `json:"D4,omitempty"`
	D5                int     `json:"D5,omitempty"`
	D6                int     `json:"D6,omitempty"`
	D7                int     `json:"D7,omitempty"`
	D8                int     `json:"D8,omitempty"`
	D9                int     `json:"D9,omitempty"`
	D10               int     `json:"D10,omitempty"`
	D11               int     `json:"D11,omitempty"`
	D12               int     `json:"D12,omitempty"`
	D13               int     `json:"D13,omitempty"`
	D14               int     `json:"D14,omitempty"`
	D15               int     `json:"D15,omitempty"`
	D16               int     `json:"D16,omitempty"`
	D17               int     `json:"D17,omitempty"`
	D18               int     `json:"D18,omitempty"`
	D19               int     `json:"D19,omitempty"`
	D20               int     `json:"D20,omitempty"`
	D21               int     `json:"D21,omitempty"`
	D22               int     `json:"D22,omitempty"`
	D23               int     `json:"D23,omitempty"`
	D24               int     `json:"D24,omitempty"`
	D25               int     `json:"D25,omitempty"`
	D26               int     `json:"D26,omitempty"`
	D27               int     `json:"D27,omitempty"`
	D28               int     `json:"D28,omitempty"`
	D29               int     `json:"D29,omitempty"`
	D30               int     `json:"D30,omitempty"`
	D31               int     `json:"D31,omitempty"`
	D32               int     `json:"D32,omitempty"`
	D33               int     `json:"D33,omitempty"`
	D34               int     `json:"D34,omitempty"`
	D35               int     `json:"D35,omitempty"`
	D36               int     `json:"D36,omitempty"`
	D37               int     `json:"D37,omitempty"`
	D38               int     `json:"D38,omitempty"`
	D39               int     `json:"D39,omitempty"`
	D40               int     `json:"D40,omitempty"`
	D41               int     `json:"D41,omitempty"`
	D42               int     `json:"D42,omitempty"`
	D43               int     `json:"D43,omitempty"`
	D44               int     `json:"D44,omitempty"`
	D45               int     `json:"D45,omitempty"`
	D46               int     `json:"D46,omitempty"`
	D47               int     `json:"D47,omitempty"`
	D48               int     `json:"D48,omitempty"`
	D49               int     `json:"D49,omitempty"`
	D50               int     `json:"D50,omitempty"`
	D51               int     `json:"D51,omitempty"`
	D52               int     `json:"D52,omitempty"`
	D53               int     `json:"D53,omitempty"`
	D54               int     `json:"D54,omitempty"`
	D55               int     `json:"D55,omitempty"`
	D56               int     `json:"D56,omitempty"`
	A1                int     `json:"A1,omitempty"`
	A2                int     `json:"A2,omitempty"`
	A3                int     `json:"A3,omitempty"`
	A4                int     `json:"A4,omitempty"`
	VA1               int     `json:"VA1,omitempty"`
	VA2               int     `json:"VA2,omitempty"`
	VA3               int     `json:"VA3,omitempty"`
	VA4               int     `json:"VA4,omitempty"`
	VA5               int     `json:"VA5,omitempty"`
	VA6               int     `json:"VA6,omitempty"`
	VA7               int     `json:"VA7,omitempty"`
	VA8               int     `json:"VA8,omitempty"`
	VA9               int     `json:"VA9,omitempty"`
	VA10              int     `json:"VA10,omitempty"`
	VA11              int     `json:"VA11,omitempty"`
	VA12              int     `json:"VA12,omitempty"`
	VA13              int     `json:"VA13,omitempty"`
	VA14              int     `json:"VA14,omitempty"`
	VA15              int     `json:"VA15,omitempty"`
	VA16              int     `json:"VA16,omitempty"`
	VA17              int     `json:"VA17,omitempty"`
	VA18              int     `json:"VA18,omitempty"`
	VA19              int     `json:"VA19,omitempty"`
	VA20              int     `json:"VA20,omitempty"`
	VA21              int     `json:"VA21,omitempty"`
	VA22              int     `json:"VA22,omitempty"`
	VA23              int     `json:"VA23,omitempty"`
	VA24              int     `json:"VA24,omitempty"`
	VA25              int     `json:"VA25,omitempty"`
	VA26              int     `json:"VA26,omitempty"`
	VA27              int     `json:"VA27,omitempty"`
	VA28              int     `json:"VA28,omitempty"`
	VA29              int     `json:"VA29,omitempty"`
	VA30              int     `json:"VA30,omitempty"`
	VA31              int     `json:"VA31,omitempty"`
	VA32              int     `json:"VA32,omitempty"`
	VI1               int     `json:"VI1,omitempty"`
	VI2               int     `json:"VI2,omitempty"`
	VI3               int     `json:"VI3,omitempty"`
	VI4               int     `json:"VI4,omitempty"`
	VI5               int     `json:"VI5,omitempty"`
	VI6               int     `json:"VI6,omitempty"`
	VI7               int     `json:"VI7,omitempty"`
	VI8               int     `json:"VI8,omitempty"`
	VI9               int     `json:"VI9,omitempty"`
	VI10              int     `json:"VI10,omitempty"`
	VI11              int     `json:"VI11,omitempty"`
	VI12              int     `json:"VI12,omitempty"`
	VI13              int     `json:"VI13,omitempty"`
	VI14              int     `json:"VI14,omitempty"`
	VI15              int     `json:"VI15,omitempty"`
	VI16              int     `json:"VI16,omitempty"`
	VI17              int     `json:"VI17,omitempty"`
	VI18              int     `json:"VI18,omitempty"`
	VI19              int     `json:"VI19,omitempty"`
	VI20              int     `json:"VI20,omitempty"`
	VI21              int     `json:"VI21,omitempty"`
	VI22              int     `json:"VI22,omitempty"`
	VI23              int     `json:"VI23,omitempty"`
	VI24              int     `json:"VI24,omitempty"`
	VI25              int     `json:"VI25,omitempty"`
	VI26              int     `json:"VI26,omitempty"`
	VI27              int     `json:"VI27,omitempty"`
	VI28              int     `json:"VI28,omitempty"`
	VI29              int     `json:"VI29,omitempty"`
	VI30              int     `json:"VI30,omitempty"`
	VI31              int     `json:"VI31,omitempty"`
	VI32              int     `json:"VI32,omitempty"`
	VI33              int     `json:"VI33,omitempty"`
	VI34              int     `json:"VI34,omitempty"`
	VI35              int     `json:"VI35,omitempty"`
	VI36              int     `json:"VI36,omitempty"`
	VI37              int     `json:"VI37,omitempty"`
	VI38              int     `json:"VI38,omitempty"`
	VI39              int     `json:"VI39,omitempty"`
	VI40              int     `json:"VI40,omitempty"`
	VI41              int     `json:"VI41,omitempty"`
	VI42              int     `json:"VI42,omitempty"`
	VI43              int     `json:"VI43,omitempty"`
	VI44              int     `json:"VI44,omitempty"`
	VI45              int     `json:"VI45,omitempty"`
	VI46              int     `json:"VI46,omitempty"`
	VI47              int     `json:"VI47,omitempty"`
	VI48              int     `json:"VI48,omitempty"`
	VI49              int     `json:"VI49,omitempty"`
	VI50              int     `json:"VI50,omitempty"`
	VI51              int     `json:"VI51,omitempty"`
	VI52              int     `json:"VI52,omitempty"`
	VI53              int     `json:"VI53,omitempty"`
	VI54              int     `json:"VI54,omitempty"`
	VI55              int     `json:"VI55,omitempty"`
	VI56              int     `json:"VI56,omitempty"`
	VI57              int     `json:"VI57,omitempty"`
	VI58              int     `json:"VI58,omitempty"`
	VI59              int     `json:"VI59,omitempty"`
	VI60              int     `json:"VI60,omitempty"`
	VI61              int     `json:"VI61,omitempty"`
	VI62              int     `json:"VI62,omitempty"`
	VI63              int     `json:"VI63,omitempty"`
	VI64              int     `json:"VI64,omitempty"`
	VI65              int     `json:"VI65,omitempty"`
	VI66              int     `json:"VI66,omitempty"`
	VI67              int     `json:"VI67,omitempty"`
	VI68              int     `json:"VI68,omitempty"`
	VI69              int     `json:"VI69,omitempty"`
	VI70              int     `json:"VI70,omitempty"`
	VI71              int     `json:"VI71,omitempty"`
	VI72              int     `json:"VI72,omitempty"`
	VI73              int     `json:"VI73,omitempty"`
	VI74              int     `json:"VI74,omitempty"`
	VI75              int     `json:"VI75,omitempty"`
	VI76              int     `json:"VI76,omitempty"`
	VI77              int     `json:"VI77,omitempty"`
	VI78              int     `json:"VI78,omitempty"`
	VI79              int     `json:"VI79,omitempty"`
	VI80              int     `json:"VI80,omitempty"`
	VI81              int     `json:"VI81,omitempty"`
	VI82              int     `json:"VI82,omitempty"`
	VI83              int     `json:"VI83,omitempty"`
	VI84              int     `json:"VI84,omitempty"`
	VI85              int     `json:"VI85,omitempty"`
	VI86              int     `json:"VI86,omitempty"`
	VI87              int     `json:"VI87,omitempty"`
	VI88              int     `json:"VI88,omitempty"`
	VI89              int     `json:"VI89,omitempty"`
	VI90              int     `json:"VI90,omitempty"`
	VI91              int     `json:"VI91,omitempty"`
	VI92              int     `json:"VI92,omitempty"`
	VI93              int     `json:"VI93,omitempty"`
	VI94              int     `json:"VI94,omitempty"`
	VI95              int     `json:"VI95,omitempty"`
	VI96              int     `json:"VI96,omitempty"`
	VI97              int     `json:"VI97,omitempty"`
	VI98              int     `json:"VI98,omitempty"`
	VI99              int     `json:"VI99,omitempty"`
	VI100             int     `json:"VI100,omitempty"`
	VI101             int     `json:"VI101,omitempty"`
	VI102             int     `json:"VI102,omitempty"`
	VI103             int     `json:"VI103,omitempty"`
	VI104             int     `json:"VI104,omitempty"`
	VI105             int     `json:"VI105,omitempty"`
	VI106             int     `json:"VI106,omitempty"`
	VI107             int     `json:"VI107,omitempty"`
	VI108             int     `json:"VI108,omitempty"`
	VI109             int     `json:"VI109,omitempty"`
	VI110             int     `json:"VI110,omitempty"`
	VI111             int     `json:"VI111,omitempty"`
	VI112             int     `json:"VI112,omitempty"`
	VI113             int     `json:"VI113,omitempty"`
	VI114             int     `json:"VI114,omitempty"`
	VI115             int     `json:"VI115,omitempty"`
	VI116             int     `json:"VI116,omitempty"`
	VI117             int     `json:"VI117,omitempty"`
	VI118             int     `json:"VI118,omitempty"`
	VI119             int     `json:"VI119,omitempty"`
	VI120             int     `json:"VI120,omitempty"`
	VI121             int     `json:"VI121,omitempty"`
	VI122             int     `json:"VI122,omitempty"`
	VI123             int     `json:"VI123,omitempty"`
	VI124             int     `json:"VI124,omitempty"`
	VI125             int     `json:"VI125,omitempty"`
	VI126             int     `json:"VI126,omitempty"`
	VI127             int     `json:"VI127,omitempty"`
	VI128             int     `json:"VI128,omitempty"`
	VO1               int     `json:"VO1,omitempty"`
	VO2               int     `json:"VO2,omitempty"`
	VO3               int     `json:"VO3,omitempty"`
	VO4               int     `json:"VO4,omitempty"`
	VO5               int     `json:"VO5,omitempty"`
	VO6               int     `json:"VO6,omitempty"`
	VO7               int     `json:"VO7,omitempty"`
	VO8               int     `json:"VO8,omitempty"`
	VO9               int     `json:"VO9,omitempty"`
	VO10              int     `json:"VO10,omitempty"`
	VO11              int     `json:"VO11,omitempty"`
	VO12              int     `json:"VO12,omitempty"`
	VO13              int     `json:"VO13,omitempty"`
	VO14              int     `json:"VO14,omitempty"`
	VO15              int     `json:"VO15,omitempty"`
	VO16              int     `json:"VO16,omitempty"`
	VO17              int     `json:"VO17,omitempty"`
	VO18              int     `json:"VO18,omitempty"`
	VO19              int     `json:"VO19,omitempty"`
	VO20              int     `json:"VO20,omitempty"`
	VO21              int     `json:"VO21,omitempty"`
	VO22              int     `json:"VO22,omitempty"`
	VO23              int     `json:"VO23,omitempty"`
	VO24              int     `json:"VO24,omitempty"`
	VO25              int     `json:"VO25,omitempty"`
	VO26              int     `json:"VO26,omitempty"`
	VO27              int     `json:"VO27,omitempty"`
	VO28              int     `json:"VO28,omitempty"`
	VO29              int     `json:"VO29,omitempty"`
	VO30              int     `json:"VO30,omitempty"`
	VO31              int     `json:"VO31,omitempty"`
	VO32              int     `json:"VO32,omitempty"`
	VO33              int     `json:"VO33,omitempty"`
	VO34              int     `json:"VO34,omitempty"`
	VO35              int     `json:"VO35,omitempty"`
	VO36              int     `json:"VO36,omitempty"`
	VO37              int     `json:"VO37,omitempty"`
	VO38              int     `json:"VO38,omitempty"`
	VO39              int     `json:"VO39,omitempty"`
	VO40              int     `json:"VO40,omitempty"`
	VO41              int     `json:"VO41,omitempty"`
	VO42              int     `json:"VO42,omitempty"`
	VO43              int     `json:"VO43,omitempty"`
	VO44              int     `json:"VO44,omitempty"`
	VO45              int     `json:"VO45,omitempty"`
	VO46              int     `json:"VO46,omitempty"`
	VO47              int     `json:"VO47,omitempty"`
	VO48              int     `json:"VO48,omitempty"`
	VO49              int     `json:"VO49,omitempty"`
	VO50              int     `json:"VO50,omitempty"`
	VO51              int     `json:"VO51,omitempty"`
	VO52              int     `json:"VO52,omitempty"`
	VO53              int     `json:"VO53,omitempty"`
	VO54              int     `json:"VO54,omitempty"`
	VO55              int     `json:"VO55,omitempty"`
	VO56              int     `json:"VO56,omitempty"`
	VO57              int     `json:"VO57,omitempty"`
	VO58              int     `json:"VO58,omitempty"`
	VO59              int     `json:"VO59,omitempty"`
	VO60              int     `json:"VO60,omitempty"`
	VO61              int     `json:"VO61,omitempty"`
	VO62              int     `json:"VO62,omitempty"`
	VO63              int     `json:"VO63,omitempty"`
	VO64              int     `json:"VO64,omitempty"`
	VO65              int     `json:"VO65,omitempty"`
	VO66              int     `json:"VO66,omitempty"`
	VO67              int     `json:"VO67,omitempty"`
	VO68              int     `json:"VO68,omitempty"`
	VO69              int     `json:"VO69,omitempty"`
	VO70              int     `json:"VO70,omitempty"`
	VO71              int     `json:"VO71,omitempty"`
	VO72              int     `json:"VO72,omitempty"`
	VO73              int     `json:"VO73,omitempty"`
	VO74              int     `json:"VO74,omitempty"`
	VO75              int     `json:"VO75,omitempty"`
	VO76              int     `json:"VO76,omitempty"`
	VO77              int     `json:"VO77,omitempty"`
	VO78              int     `json:"VO78,omitempty"`
	VO79              int     `json:"VO79,omitempty"`
	VO80              int     `json:"VO80,omitempty"`
	VO81              int     `json:"VO81,omitempty"`
	VO82              int     `json:"VO82,omitempty"`
	VO83              int     `json:"VO83,omitempty"`
	VO84              int     `json:"VO84,omitempty"`
	VO85              int     `json:"VO85,omitempty"`
	VO86              int     `json:"VO86,omitempty"`
	VO87              int     `json:"VO87,omitempty"`
	VO88              int     `json:"VO88,omitempty"`
	VO89              int     `json:"VO89,omitempty"`
	VO90              int     `json:"VO90,omitempty"`
	VO91              int     `json:"VO91,omitempty"`
	VO92              int     `json:"VO92,omitempty"`
	VO93              int     `json:"VO93,omitempty"`
	VO94              int     `json:"VO94,omitempty"`
	VO95              int     `json:"VO95,omitempty"`
	VO96              int     `json:"VO96,omitempty"`
	VO97              int     `json:"VO97,omitempty"`
	VO98              int     `json:"VO98,omitempty"`
	VO99              int     `json:"VO99,omitempty"`
	VO100             int     `json:"VO100,omitempty"`
	VO101             int     `json:"VO101,omitempty"`
	VO102             int     `json:"VO102,omitempty"`
	VO103             int     `json:"VO103,omitempty"`
	VO104             int     `json:"VO104,omitempty"`
	VO105             int     `json:"VO105,omitempty"`
	VO106             int     `json:"VO106,omitempty"`
	VO107             int     `json:"VO107,omitempty"`
	VO108             int     `json:"VO108,omitempty"`
	VO109             int     `json:"VO109,omitempty"`
	VO110             int     `json:"VO110,omitempty"`
	VO111             int     `json:"VO111,omitempty"`
	VO112             int     `json:"VO112,omitempty"`
	VO113             int     `json:"VO113,omitempty"`
	VO114             int     `json:"VO114,omitempty"`
	VO115             int     `json:"VO115,omitempty"`
	VO116             int     `json:"VO116,omitempty"`
	VO117             int     `json:"VO117,omitempty"`
	VO118             int     `json:"VO118,omitempty"`
	VO119             int     `json:"VO119,omitempty"`
	VO120             int     `json:"VO120,omitempty"`
	VO121             int     `json:"VO121,omitempty"`
	VO122             int     `json:"VO122,omitempty"`
	VO123             int     `json:"VO123,omitempty"`
	VO124             int     `json:"VO124,omitempty"`
	VO125             int     `json:"VO125,omitempty"`
	VO126             int     `json:"VO126,omitempty"`
	VO127             int     `json:"VO127,omitempty"`
	VO128             int     `json:"VO128,omitempty"`
	PW1               int     `json:"PW1,omitempty"`
	PW2               int     `json:"PW2,omitempty"`
	PW3               int     `json:"PW3,omitempty"`
	PW4               int     `json:"PW4,omitempty"`
	PW5               int     `json:"PW5,omitempty"`
	PW6               int     `json:"PW6,omitempty"`
	PW7               int     `json:"PW7,omitempty"`
	PW8               int     `json:"PW8,omitempty"`
	PW9               int     `json:"PW9,omitempty"`
	PW10              int     `json:"PW10,omitempty"`
	PW11              int     `json:"PW11,omitempty"`
	PW12              int     `json:"PW12,omitempty"`
	PW13              int     `json:"PW13,omitempty"`
	PW14              int     `json:"PW14,omitempty"`
	PW15              int     `json:"PW15,omitempty"`
	PW16              int     `json:"PW16,omitempty"`
	PW17              int     `json:"PW17,omitempty"`
	PW18              int     `json:"PW18,omitempty"`
	PW19              int     `json:"PW19,omitempty"`
	PW20              int     `json:"PW20,omitempty"`
	PW21              int     `json:"PW21,omitempty"`
	PW22              int     `json:"PW22,omitempty"`
	PW23              int     `json:"PW23,omitempty"`
	PW24              int     `json:"PW24,omitempty"`
	PW25              int     `json:"PW25,omitempty"`
	PW26              int     `json:"PW26,omitempty"`
	PW27              int     `json:"PW27,omitempty"`
	PW28              int     `json:"PW28,omitempty"`
	PW29              int     `json:"PW29,omitempty"`
	PW30              int     `json:"PW30,omitempty"`
	PW31              int     `json:"PW31,omitempty"`
	PW32              int     `json:"PW32,omitempty"`
	VR11              int     `json:"VR1-1,omitempty"`
	VR12              int     `json:"VR1-2,omitempty"`
	VR13              int     `json:"VR1-3,omitempty"`
	VR14              int     `json:"VR1-4,omitempty"`
	VR21              int     `json:"VR2-1,omitempty"`
	VR22              int     `json:"VR2-2,omitempty"`
	VR23              int     `json:"VR2-3,omitempty"`
	VR24              int     `json:"VR2-4,omitempty"`
	VR31              int     `json:"VR3-1,omitempty"`
	VR32              int     `json:"VR3-2,omitempty"`
	VR33              int     `json:"VR3-3,omitempty"`
	VR34              int     `json:"VR3-4,omitempty"`
	VR41              int     `json:"VR4-1,omitempty"`
	VR42              int     `json:"VR4-2,omitempty"`
	VR43              int     `json:"VR4-3,omitempty"`
	VR44              int     `json:"VR4-4,omitempty"`
	VR51              int     `json:"VR5-1,omitempty"`
	VR52              int     `json:"VR5-2,omitempty"`
	VR53              int     `json:"VR5-3,omitempty"`
	VR54              int     `json:"VR5-4,omitempty"`
	VR61              int     `json:"VR6-1,omitempty"`
	VR62              int     `json:"VR6-2,omitempty"`
	VR63              int     `json:"VR6-3,omitempty"`
	VR64              int     `json:"VR6-4,omitempty"`
	VR71              int     `json:"VR7-1,omitempty"`
	VR72              int     `json:"VR7-2,omitempty"`
	VR73              int     `json:"VR7-3,omitempty"`
	VR74              int     `json:"VR7-4,omitempty"`
	VR81              int     `json:"VR8-1,omitempty"`
	VR82              int     `json:"VR8-2,omitempty"`
	VR83              int     `json:"VR8-3,omitempty"`
	VR84              int     `json:"VR8-4,omitempty"`
	FP1Zone1          int     `json:"FP1 Zone 1,omitempty"`
	FP1Zone2          int     `json:"FP1 Zone 2,omitempty"`
	FP1Zone3          int     `json:"FP1 Zone 3,omitempty"`
	FP1Zone4          int     `json:"FP1 Zone 4,omitempty"`
	FP2Zone1          int     `json:"FP2 Zone 1,omitempty"`
	FP2Zone2          int     `json:"FP2 Zone 2,omitempty"`
	FP2Zone3          int     `json:"FP2 Zone 3,omitempty"`
	FP2Zone4          int     `json:"FP2 Zone 4,omitempty"`
	FP3Zone1          int     `json:"FP3 Zone 1,omitempty"`
	FP3Zone2          int     `json:"FP3 Zone 2,omitempty"`
	FP3Zone3          int     `json:"FP3 Zone 3,omitempty"`
	FP3Zone4          int     `json:"FP3 Zone 4,omitempty"`
	FP4Zone1          int     `json:"FP4 Zone 1,omitempty"`
	FP4Zone2          int     `json:"FP4 Zone 2,omitempty"`
	FP4Zone3          int     `json:"FP4 Zone 3,omitempty"`
	FP4Zone4          int     `json:"FP4 Zone 4,omitempty"`
	THL1TEMP          float64 `json:"THL1-TEMP,omitempty"`
	THL1HUM           float64 `json:"THL1-HUM,omitempty"`
	THL1LUM           int     `json:"THL1-LUM,omitempty"`
	THL2TEMP          float64 `json:"THL2-TEMP,omitempty"`
	THL2HUM           float64 `json:"THL2-HUM,omitempty"`
	THL2LUM           int     `json:"THL2-LUM,omitempty"`
	THL3TEMP          float64 `json:"THL3-TEMP,omitempty"`
	THL3HUM           float64 `json:"THL3-HUM,omitempty"`
	THL3LUM           int     `json:"THL3-LUM,omitempty"`
	THL4TEMP          float64 `json:"THL4-TEMP,omitempty"`
	THL4HUM           float64 `json:"THL4-HUM,omitempty"`
	THL4LUM           int     `json:"THL4-LUM,omitempty"`
	THL5TEMP          float64 `json:"THL5-TEMP,omitempty"`
	THL5HUM           float64 `json:"THL5-HUM,omitempty"`
	THL5LUM           int     `json:"THL5-LUM,omitempty"`
	THL6TEMP          float64 `json:"THL6-TEMP,omitempty"`
	THL6HUM           float64 `json:"THL6-HUM,omitempty"`
	THL6LUM           int     `json:"THL6-LUM,omitempty"`
	THL7TEMP          float64 `json:"THL7-TEMP,omitempty"`
	THL7HUM           float64 `json:"THL7-HUM,omitempty"`
	THL7LUM           int     `json:"THL7-LUM,omitempty"`
	THL8TEMP          float64 `json:"THL8-TEMP,omitempty"`
	THL8HUM           float64 `json:"THL8-HUM,omitempty"`
	THL8LUM           int     `json:"THL8-LUM,omitempty"`
	THL9TEMP          float64 `json:"THL9-TEMP,omitempty"`
	THL9HUM           float64 `json:"THL9-HUM,omitempty"`
	THL9LUM           int     `json:"THL9-LUM,omitempty"`
	THL10TEMP         float64 `json:"THL10-TEMP,omitempty"`
	THL10HUM          float64 `json:"THL10-HUM,omitempty"`
	THL10LUM          int     `json:"THL10-LUM,omitempty"`
	THL11TEMP         float64 `json:"THL11-TEMP,omitempty"`
	THL11HUM          float64 `json:"THL11-HUM,omitempty"`
	THL11LUM          int     `json:"THL11-LUM,omitempty"`
	THL12TEMP         float64 `json:"THL12-TEMP,omitempty"`
	THL12HUM          float64 `json:"THL12-HUM,omitempty"`
	THL12LUM          int     `json:"THL12-LUM,omitempty"`
	THL13TEMP         float64 `json:"THL13-TEMP,omitempty"`
	THL13HUM          float64 `json:"THL13-HUM,omitempty"`
	THL13LUM          int     `json:"THL13-LUM,omitempty"`
	THL14TEMP         float64 `json:"THL14-TEMP,omitempty"`
	THL14HUM          float64 `json:"THL14-HUM,omitempty"`
	THL14LUM          int     `json:"THL14-LUM,omitempty"`
	ENOSWITCH1        int     `json:"ENO SWITCH1,omitempty"`
	ENOSWITCH2        int     `json:"ENO SWITCH2,omitempty"`
	ENOSWITCH3        int     `json:"ENO SWITCH3,omitempty"`
	ENOSWITCH4        int     `json:"ENO SWITCH4,omitempty"`
	ENOSWITCH5        int     `json:"ENO SWITCH5,omitempty"`
	ENOSWITCH6        int     `json:"ENO SWITCH6,omitempty"`
	ENOSWITCH7        int     `json:"ENO SWITCH7,omitempty"`
	ENOSWITCH8        int     `json:"ENO SWITCH8,omitempty"`
	ENOSWITCH9        int     `json:"ENO SWITCH9,omitempty"`
	ENOSWITCH10       int     `json:"ENO SWITCH10,omitempty"`
	ENOSWITCH11       int     `json:"ENO SWITCH11,omitempty"`
	ENOSWITCH12       int     `json:"ENO SWITCH12,omitempty"`
	ENOSWITCH13       int     `json:"ENO SWITCH13,omitempty"`
	ENOSWITCH14       int     `json:"ENO SWITCH14,omitempty"`
	ENOSWITCH15       int     `json:"ENO SWITCH15,omitempty"`
	ENOSWITCH16       int     `json:"ENO SWITCH16,omitempty"`
	ENOSWITCH17       int     `json:"ENO SWITCH17,omitempty"`
	ENOSWITCH18       int     `json:"ENO SWITCH18,omitempty"`
	ENOSWITCH19       int     `json:"ENO SWITCH19,omitempty"`
	ENOSWITCH20       int     `json:"ENO SWITCH20,omitempty"`
	ENOSWITCH21       int     `json:"ENO SWITCH21,omitempty"`
	ENOSWITCH22       int     `json:"ENO SWITCH22,omitempty"`
	ENOSWITCH23       int     `json:"ENO SWITCH23,omitempty"`
	ENOSWITCH24       int     `json:"ENO SWITCH24,omitempty"`
	ENOSWITCH25       int     `json:"ENO SWITCH25,omitempty"`
	ENOSWITCH26       int     `json:"ENO SWITCH26,omitempty"`
	ENOSWITCH27       int     `json:"ENO SWITCH27,omitempty"`
	ENOSWITCH28       int     `json:"ENO SWITCH28,omitempty"`
	ENOSWITCH29       int     `json:"ENO SWITCH29,omitempty"`
	ENOSWITCH30       int     `json:"ENO SWITCH30,omitempty"`
	ENOSWITCH31       int     `json:"ENO SWITCH31,omitempty"`
	ENOSWITCH32       int     `json:"ENO SWITCH32,omitempty"`
	ENOSWITCH33       int     `json:"ENO SWITCH33,omitempty"`
	ENOSWITCH34       int     `json:"ENO SWITCH34,omitempty"`
	ENOSWITCH35       int     `json:"ENO SWITCH35,omitempty"`
	ENOSWITCH36       int     `json:"ENO SWITCH36,omitempty"`
	ENOSWITCH37       int     `json:"ENO SWITCH37,omitempty"`
	ENOSWITCH38       int     `json:"ENO SWITCH38,omitempty"`
	ENOSWITCH39       int     `json:"ENO SWITCH39,omitempty"`
	ENOSWITCH40       int     `json:"ENO SWITCH40,omitempty"`
	ENOSWITCH41       int     `json:"ENO SWITCH41,omitempty"`
	ENOSWITCH42       int     `json:"ENO SWITCH42,omitempty"`
	ENOSWITCH43       int     `json:"ENO SWITCH43,omitempty"`
	ENOSWITCH44       int     `json:"ENO SWITCH44,omitempty"`
	ENOSWITCH45       int     `json:"ENO SWITCH45,omitempty"`
	ENOSWITCH46       int     `json:"ENO SWITCH46,omitempty"`
	ENOSWITCH47       int     `json:"ENO SWITCH47,omitempty"`
	ENOSWITCH48       int     `json:"ENO SWITCH48,omitempty"`
	ENOSWITCH49       int     `json:"ENO SWITCH49,omitempty"`
	ENOSWITCH50       int     `json:"ENO SWITCH50,omitempty"`
	ENOSWITCH51       int     `json:"ENO SWITCH51,omitempty"`
	ENOSWITCH52       int     `json:"ENO SWITCH52,omitempty"`
	ENOSWITCH53       int     `json:"ENO SWITCH53,omitempty"`
	ENOSWITCH54       int     `json:"ENO SWITCH54,omitempty"`
	ENOSWITCH55       int     `json:"ENO SWITCH55,omitempty"`
	ENOSWITCH56       int     `json:"ENO SWITCH56,omitempty"`
	ENOSWITCH57       int     `json:"ENO SWITCH57,omitempty"`
	ENOSWITCH58       int     `json:"ENO SWITCH58,omitempty"`
	ENOSWITCH59       int     `json:"ENO SWITCH59,omitempty"`
	ENOSWITCH60       int     `json:"ENO SWITCH60,omitempty"`
	ENOSWITCH61       int     `json:"ENO SWITCH61,omitempty"`
	ENOSWITCH62       int     `json:"ENO SWITCH62,omitempty"`
	ENOSWITCH63       int     `json:"ENO SWITCH63,omitempty"`
	ENOSWITCH64       int     `json:"ENO SWITCH64,omitempty"`
	ENOSWITCH65       int     `json:"ENO SWITCH65,omitempty"`
	ENOSWITCH66       int     `json:"ENO SWITCH66,omitempty"`
	ENOSWITCH67       int     `json:"ENO SWITCH67,omitempty"`
	ENOSWITCH68       int     `json:"ENO SWITCH68,omitempty"`
	ENOSWITCH69       int     `json:"ENO SWITCH69,omitempty"`
	ENOSWITCH70       int     `json:"ENO SWITCH70,omitempty"`
	ENOSWITCH71       int     `json:"ENO SWITCH71,omitempty"`
	ENOSWITCH72       int     `json:"ENO SWITCH72,omitempty"`
	ENOSWITCH73       int     `json:"ENO SWITCH73,omitempty"`
	ENOSWITCH74       int     `json:"ENO SWITCH74,omitempty"`
	ENOSWITCH75       int     `json:"ENO SWITCH75,omitempty"`
	ENOSWITCH76       int     `json:"ENO SWITCH76,omitempty"`
	ENOSWITCH77       int     `json:"ENO SWITCH77,omitempty"`
	ENOSWITCH78       int     `json:"ENO SWITCH78,omitempty"`
	ENOSWITCH79       int     `json:"ENO SWITCH79,omitempty"`
	ENOSWITCH80       int     `json:"ENO SWITCH80,omitempty"`
	ENOSWITCH81       int     `json:"ENO SWITCH81,omitempty"`
	ENOSWITCH82       int     `json:"ENO SWITCH82,omitempty"`
	ENOSWITCH83       int     `json:"ENO SWITCH83,omitempty"`
	ENOSWITCH84       int     `json:"ENO SWITCH84,omitempty"`
	ENOSWITCH85       int     `json:"ENO SWITCH85,omitempty"`
	ENOSWITCH86       int     `json:"ENO SWITCH86,omitempty"`
	ENOSWITCH87       int     `json:"ENO SWITCH87,omitempty"`
	ENOSWITCH88       int     `json:"ENO SWITCH88,omitempty"`
	ENOSWITCH89       int     `json:"ENO SWITCH89,omitempty"`
	ENOSWITCH90       int     `json:"ENO SWITCH90,omitempty"`
	ENOSWITCH91       int     `json:"ENO SWITCH91,omitempty"`
	ENOSWITCH92       int     `json:"ENO SWITCH92,omitempty"`
	ENOSWITCH93       int     `json:"ENO SWITCH93,omitempty"`
	ENOSWITCH94       int     `json:"ENO SWITCH94,omitempty"`
	ENOSWITCH95       int     `json:"ENO SWITCH95,omitempty"`
	ENOSWITCH96       int     `json:"ENO SWITCH96,omitempty"`
	ENOCONTACT1       int     `json:"ENO CONTACT1,omitempty"`
	ENOCONTACT2       int     `json:"ENO CONTACT2,omitempty"`
	ENOCONTACT3       int     `json:"ENO CONTACT3,omitempty"`
	ENOCONTACT4       int     `json:"ENO CONTACT4,omitempty"`
	ENOCONTACT5       int     `json:"ENO CONTACT5,omitempty"`
	ENOCONTACT6       int     `json:"ENO CONTACT6,omitempty"`
	ENOCONTACT7       int     `json:"ENO CONTACT7,omitempty"`
	ENOCONTACT8       int     `json:"ENO CONTACT8,omitempty"`
	ENOCONTACT9       int     `json:"ENO CONTACT9,omitempty"`
	ENOCONTACT10      int     `json:"ENO CONTACT10,omitempty"`
	ENOCONTACT11      int     `json:"ENO CONTACT11,omitempty"`
	ENOCONTACT12      int     `json:"ENO CONTACT12,omitempty"`
	ENOCONTACT13      int     `json:"ENO CONTACT13,omitempty"`
	ENOCONTACT14      int     `json:"ENO CONTACT14,omitempty"`
	ENOCONTACT15      int     `json:"ENO CONTACT15,omitempty"`
	ENOCONTACT16      int     `json:"ENO CONTACT16,omitempty"`
	ENOCONTACT17      int     `json:"ENO CONTACT17,omitempty"`
	ENOCONTACT18      int     `json:"ENO CONTACT18,omitempty"`
	ENOCONTACT19      int     `json:"ENO CONTACT19,omitempty"`
	ENOCONTACT20      int     `json:"ENO CONTACT20,omitempty"`
	ENOCONTACT21      int     `json:"ENO CONTACT21,omitempty"`
	ENOCONTACT22      int     `json:"ENO CONTACT22,omitempty"`
	ENOCONTACT23      int     `json:"ENO CONTACT23,omitempty"`
	ENOCONTACT24      int     `json:"ENO CONTACT24,omitempty"`
	ENOACTIONNEUR1    int     `json:"ENO ACTIONNEUR1,omitempty"`
	ENOACTIONNEUR2    int     `json:"ENO ACTIONNEUR2,omitempty"`
	ENOACTIONNEUR3    int     `json:"ENO ACTIONNEUR3,omitempty"`
	ENOACTIONNEUR4    int     `json:"ENO ACTIONNEUR4,omitempty"`
	ENOACTIONNEUR5    int     `json:"ENO ACTIONNEUR5,omitempty"`
	ENOACTIONNEUR6    int     `json:"ENO ACTIONNEUR6,omitempty"`
	ENOACTIONNEUR7    int     `json:"ENO ACTIONNEUR7,omitempty"`
	ENOACTIONNEUR8    int     `json:"ENO ACTIONNEUR8,omitempty"`
	ENOACTIONNEUR9    int     `json:"ENO ACTIONNEUR9,omitempty"`
	ENOACTIONNEUR10   int     `json:"ENO ACTIONNEUR10,omitempty"`
	ENOACTIONNEUR11   int     `json:"ENO ACTIONNEUR11,omitempty"`
	ENOACTIONNEUR12   int     `json:"ENO ACTIONNEUR12,omitempty"`
	ENOACTIONNEUR13   int     `json:"ENO ACTIONNEUR13,omitempty"`
	ENOACTIONNEUR14   int     `json:"ENO ACTIONNEUR14,omitempty"`
	ENOACTIONNEUR15   int     `json:"ENO ACTIONNEUR15,omitempty"`
	ENOACTIONNEUR16   int     `json:"ENO ACTIONNEUR16,omitempty"`
	ENOACTIONNEUR17   int     `json:"ENO ACTIONNEUR17,omitempty"`
	ENOACTIONNEUR18   int     `json:"ENO ACTIONNEUR18,omitempty"`
	ENOACTIONNEUR19   int     `json:"ENO ACTIONNEUR19,omitempty"`
	ENOACTIONNEUR20   int     `json:"ENO ACTIONNEUR20,omitempty"`
	ENOACTIONNEUR21   int     `json:"ENO ACTIONNEUR21,omitempty"`
	ENOACTIONNEUR22   int     `json:"ENO ACTIONNEUR22,omitempty"`
	ENOACTIONNEUR23   int     `json:"ENO ACTIONNEUR23,omitempty"`
	ENOACTIONNEUR24   int     `json:"ENO ACTIONNEUR24,omitempty"`
	ENOANALOG17       int     `json:"ENO ANALOG17,omitempty"`
	ENOANALOG18       int     `json:"ENO ANALOG18,omitempty"`
	ENOANALOG19       int     `json:"ENO ANALOG19,omitempty"`
	ENOANALOG20       int     `json:"ENO ANALOG20,omitempty"`
	ENOANALOG21       int     `json:"ENO ANALOG21,omitempty"`
	ENOANALOG22       int     `json:"ENO ANALOG22,omitempty"`
	ENOANALOG23       int     `json:"ENO ANALOG23,omitempty"`
	ENOANALOG24       int     `json:"ENO ANALOG24,omitempty"`
	ENOANALOG25       int     `json:"ENO ANALOG25,omitempty"`
	ENOANALOG26       int     `json:"ENO ANALOG26,omitempty"`
	ENOANALOG27       int     `json:"ENO ANALOG27,omitempty"`
	ENOANALOG28       int     `json:"ENO ANALOG28,omitempty"`
	ENOANALOG29       int     `json:"ENO ANALOG29,omitempty"`
	ENOANALOG30       int     `json:"ENO ANALOG30,omitempty"`
	ENOANALOG31       int     `json:"ENO ANALOG31,omitempty"`
	ENOANALOG32       int     `json:"ENO ANALOG32,omitempty"`
	ENOANALOG33       int     `json:"ENO ANALOG33,omitempty"`
	ENOANALOG34       int     `json:"ENO ANALOG34,omitempty"`
	ENOANALOG35       int     `json:"ENO ANALOG35,omitempty"`
	ENOANALOG36       int     `json:"ENO ANALOG36,omitempty"`
	ENOANALOG37       int     `json:"ENO ANALOG37,omitempty"`
	ENOANALOG38       int     `json:"ENO ANALOG38,omitempty"`
	ENOANALOG39       int     `json:"ENO ANALOG39,omitempty"`
	ENOANALOG40       int     `json:"ENO ANALOG40,omitempty"`
	ENOVOLETROULANT1  int     `json:"ENO VOLET ROULANT1,omitempty"`
	ENOVOLETROULANT2  int     `json:"ENO VOLET ROULANT2,omitempty"`
	ENOVOLETROULANT3  int     `json:"ENO VOLET ROULANT3,omitempty"`
	ENOVOLETROULANT4  int     `json:"ENO VOLET ROULANT4,omitempty"`
	ENOVOLETROULANT5  int     `json:"ENO VOLET ROULANT5,omitempty"`
	ENOVOLETROULANT6  int     `json:"ENO VOLET ROULANT6,omitempty"`
	ENOVOLETROULANT7  int     `json:"ENO VOLET ROULANT7,omitempty"`
	ENOVOLETROULANT8  int     `json:"ENO VOLET ROULANT8,omitempty"`
	ENOVOLETROULANT9  int     `json:"ENO VOLET ROULANT9,omitempty"`
	ENOVOLETROULANT10 int     `json:"ENO VOLET ROULANT10,omitempty"`
	ENOVOLETROULANT11 int     `json:"ENO VOLET ROULANT11,omitempty"`
	ENOVOLETROULANT12 int     `json:"ENO VOLET ROULANT12,omitempty"`
	ENOVOLETROULANT13 int     `json:"ENO VOLET ROULANT13,omitempty"`
	ENOVOLETROULANT14 int     `json:"ENO VOLET ROULANT14,omitempty"`
	ENOVOLETROULANT15 int     `json:"ENO VOLET ROULANT15,omitempty"`
	ENOVOLETROULANT16 int     `json:"ENO VOLET ROULANT16,omitempty"`
	ENOVOLETROULANT17 int     `json:"ENO VOLET ROULANT17,omitempty"`
	ENOVOLETROULANT18 int     `json:"ENO VOLET ROULANT18,omitempty"`
	ENOVOLETROULANT19 int     `json:"ENO VOLET ROULANT19,omitempty"`
	ENOVOLETROULANT20 int     `json:"ENO VOLET ROULANT20,omitempty"`
	ENOVOLETROULANT21 int     `json:"ENO VOLET ROULANT21,omitempty"`
	ENOVOLETROULANT22 int     `json:"ENO VOLET ROULANT22,omitempty"`
	ENOVOLETROULANT23 int     `json:"ENO VOLET ROULANT23,omitempty"`
	ENOVOLETROULANT24 int     `json:"ENO VOLET ROULANT24,omitempty"`
	G1                struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G1,omitempty"`
	G2 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G2,omitempty"`
	G3 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G3,omitempty"`
	G4 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G4,omitempty"`
	G5 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G5,omitempty"`
	G6 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G6,omitempty"`
	G7 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G7,omitempty"`
	G8 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G8,omitempty"`
	G9 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G9,omitempty"`
	G10 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G10,omitempty"`
	G11 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G11,omitempty"`
	G12 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G12,omitempty"`
	G13 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G13,omitempty"`
	G14 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G14,omitempty"`
	G15 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G15,omitempty"`
	G16 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G16,omitempty"`
	G17 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G17,omitempty"`
	G18 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G18,omitempty"`
	G19 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G19,omitempty"`
	G20 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G20,omitempty"`
	G21 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G21,omitempty"`
	G22 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G22,omitempty"`
	G23 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G23,omitempty"`
	G24 struct {
		Etat   string `json:"Etat,omitempty"`
		Valeur int    `json:"Valeur,omitempty"`
	} `json:"G24,omitempty"`
	T1  float64 `json:"T1,omitempty"`
	T2  float64 `json:"T2,omitempty"`
	T3  float64 `json:"T3,omitempty"`
	T4  float64 `json:"T4,omitempty"`
	T5  float64 `json:"T5,omitempty"`
	T6  float64 `json:"T6,omitempty"`
	T7  float64 `json:"T7,omitempty"`
	T8  float64 `json:"T8,omitempty"`
	T9  float64 `json:"T9,omitempty"`
	T10 float64 `json:"T10,omitempty"`
	T11 float64 `json:"T11,omitempty"`
	T12 float64 `json:"T12,omitempty"`
	T13 float64 `json:"T13,omitempty"`
	T14 float64 `json:"T14,omitempty"`
	T15 float64 `json:"T15,omitempty"`
	T16 float64 `json:"T16,omitempty"`
	Y1  int     `json:"Y1,omitempty"`
	Y2  int     `json:"Y2,omitempty"`
	Y3  int     `json:"Y3,omitempty"`
	Y4  int     `json:"Y4,omitempty"`
}

func (r Response) Relays() map[int]bool {
	return map[int]bool{
		1:  r.R1 == 1,
		2:  r.R2 == 1,
		3:  r.R3 == 1,
		4:  r.R4 == 1,
		5:  r.R5 == 1,
		6:  r.R6 == 1,
		7:  r.R7 == 1,
		8:  r.R8 == 1,
		9:  r.R9 == 1,
		10: r.R10 == 1,
		11: r.R11 == 1,
		12: r.R12 == 1,
		13: r.R13 == 1,
		14: r.R14 == 1,
		15: r.R15 == 1,
		16: r.R16 == 1,
		17: r.R17 == 1,
		18: r.R18 == 1,
		19: r.R19 == 1,
		20: r.R20 == 1,
		21: r.R21 == 1,
		22: r.R22 == 1,
		23: r.R23 == 1,
		24: r.R24 == 1,
		25: r.R25 == 1,
		26: r.R26 == 1,
		27: r.R27 == 1,
		28: r.R28 == 1,
		29: r.R29 == 1,
		30: r.R30 == 1,
		31: r.R31 == 1,
		32: r.R32 == 1,
		33: r.R33 == 1,
		34: r.R34 == 1,
		35: r.R35 == 1,
		36: r.R36 == 1,
		37: r.R37 == 1,
		38: r.R38 == 1,
		39: r.R39 == 1,
		40: r.R40 == 1,
		41: r.R41 == 1,
		42: r.R42 == 1,
		43: r.R43 == 1,
		44: r.R44 == 1,
		45: r.R45 == 1,
		46: r.R46 == 1,
		47: r.R47 == 1,
		48: r.R48 == 1,
		49: r.R49 == 1,
		50: r.R50 == 1,
		51: r.R51 == 1,
		52: r.R52 == 1,
		53: r.R53 == 1,
		54: r.R54 == 1,
		55: r.R55 == 1,
		56: r.R56 == 1,
	}
}
