package v4

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Controller struct {
	Address string      `json:"address" yaml:"address"`
	Port    int         `json:"port" yaml:"port"`
	APIKey  interface{} `json:"api_key" yaml:"api_key"`
}

func New(address string, port int, apiKey interface{}) Controller {
	cfg := Controller{
		Address: address,
		Port:    port,
	}

	if apiKey != nil {
		cfg.APIKey = fmt.Sprintf("%v", apiKey)
	}

	return cfg
}

func (ctr Controller) Request(cmd []string) (*Response, error) {
	if ctr.APIKey != nil {
		cmd = append([]string{fmt.Sprintf("key=%v", ctr.APIKey)}, cmd...)
	}

	url := fmt.Sprintf("http://%s:%d/api/xdevices.json?%s", ctr.Address, ctr.Port, strings.Join(cmd, "&"))

	fmt.Printf("[IPX800V4] Send %s\n", url)

	ret, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer ret.Body.Close()

	body, err := ioutil.ReadAll(ret.Body)
	if err != nil {
		return nil, err
	}

	res := &Response{}

	if err := json.Unmarshal(body, res); err != nil {
		return nil, err
	}

	return res, nil
}

func (ctr Controller) GetRelayState(ch int) (bool, error) {
	res, err := ctr.Request([]string{"Get=R"})
	if err != nil {
		return false, err
	}

	return res.Relays()[ch], nil
}

func (ctr Controller) SetRelayOn(ch int) error {
	_, err := ctr.Request([]string{fmt.Sprintf("SetR=%0.2d", ch)})
	if err != nil {
		return err
	}

	return nil
}

func (ctr Controller) SetRelayOff(ch int) error {
	_, err := ctr.Request([]string{fmt.Sprintf("ClearR=%0.2d", ch)})
	if err != nil {
		return err
	}

	return nil
}
