FROM alpine:3.10
COPY ipx800v4 /usr/local/bin/ipx800v4
CMD ["ipx800v4"]
