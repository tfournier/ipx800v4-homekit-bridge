# IPX800 V4 - HomeKit Bridge

Interact with your IPX800 V4 from the Apple ecosystem.

/!\ **Project in development** /!\ 

### Configuration
```yaml
port: "9898"                  # Optional - String - Default: 9898 
pin: "00102003"               # Optional - String - Default: 00102003
setup_id: "HOME"              # Optional - String - Default: HOME

ipx:
  address: "192.168.1.100"    # Required - String
  port: 80                    # Required - Int
  api_key: "apiKey"           # Required - String

accessories:                  # Accessories enabled
  relays:
    - id: 1
      name: "Office lighting"
    - id: 22
      name: "Office plug"
```

### Resources
- https://forum.gce-electronics.com/t/commande-api-get-all/4023
- http://gce.ovh/wiki/index.php?title=API_V4_Pilotage_par_URL
- http://gce.ovh/wiki/index.php?title=API_V4